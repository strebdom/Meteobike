#ifndef __GIT_METEOBIKE_INCLUDE_UTILS_H_
#define __GIT_METEOBIKE_INCLUDE_UTILS_H_

#include <map>
#include <regex>
#include <string>
#include <variant>

namespace utils {
using configmap = std::map<std::string, std::string>;
using sectionconfigmap = std::map<std::string, std::map<std::string, std::variant<std::string, int, double>>>;

std::string getIP();
std::string getHostname();
configmap parseConfig(std::string filename);
sectionconfigmap parseSectionConfig(std::string filename);
bool detectNumber(const std::string &str);
static inline void ltrim(std::string &s);
static inline void rtrim(std::string &s);
static inline void trim(std::string &s);
static inline std::string ltrim_copy(std::string s);
static inline std::string rtrim_copy(std::string s);
static inline std::string trim_copy(std::string s);

template <typename T> T getValueFromConfMap(const sectionconfigmap &conf, std::string section, std::string key) {
    return std::get<T>(conf.at(section).at(key));
}

} // namespace utils

#endif // __GIT_METEOBIKE_INCLUDE_UTILS_H_

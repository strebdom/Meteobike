#ifndef _CONSTS
#define _CONSTS

#include "EPD_2in7.h"
#include "GUI_BMPfile.h"
#include "GUI_Paint.h"
#include <iostream>
#include <string>

enum KEYS
{
    NOKEY = 0,
    KEY1 = 5,
    KEY2 = 6,
    KEY3 = 13,
    KEY4 = 19
};

/*constexpr int PIN = 4;
constexpr int DHT_SLEEP = 3;
constexpr int GPS_SLEEP = 1;
constexpr auto kWaitingTime{1000000};
const std::string PATH = "/home/pi/data/";
const std::string ETHLOGO = "/home/pi/git/Meteobike/Images/eth_new.bmp";*/

#endif
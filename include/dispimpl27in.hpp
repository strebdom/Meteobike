#ifndef __GIT_METEOBIKE_INCLUDE_DISPIMPL27IN_HPP_
#define __GIT_METEOBIKE_INCLUDE_DISPIMPL27IN_HPP_

#include "EPD_2in7.h"
#include "dispimpl.hpp"

class DispImpl27in : public DispImpl {
  public:
    DispImpl27in();
    void Init() const override;
    void Display() const override;
    void Clear() const override;
    void NewImage(UWORD Rotate, UWORD Color) const override;
};

#endif // __GIT_METEOBIKE_INCLUDE_DISPIMPL27IN_HPP_
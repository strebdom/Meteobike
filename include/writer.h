#pragma once

#include "measurement.h"
#include <atomic>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

class writer {
  private:
    /* data */
    std::string path, hostname, ip;
    std::string record;
    std::ofstream ofile;
    int diritems;
    void writeHeader();

  public:
    writer(const std::string &path, const std::string &hostname, const std::string &ip);
    void createRecord(const results_r &myresult);
    void writeRecord();
    std::atomic<int> counter = 0;
    void closeFile();
    ~writer();
};

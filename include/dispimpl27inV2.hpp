#ifndef __GIT_METEOBIKE_INCLUDE_DISPIMPL27V2_HPP_
#define __GIT_METEOBIKE_INCLUDE_DISPIMPL27V2_HPP_

#include "EPD_2in7_V2.h"
#include "dispimpl.hpp"
class DispImpl27inV2 : public DispImpl {
  public:
    DispImpl27inV2();
    void Init() const override;
    void Display() const override;
    void Clear() const override;
    void NewImage(uint16_t Rotate, uint16_t Color) const override;
};
#endif // __GIT_METEOBIKE_INCLUDE_DISPIMPL27V2_HPP_
#ifndef __GIT_METEOBIKE_INCLUDE_DISPLAYUPDATER_H_
#define __GIT_METEOBIKE_INCLUDE_DISPLAYUPDATER_H_

#include "consts.h"
#include "dispimpl.hpp"
#include "gpspoller.h"
#include "measurement.h"
#include "ts_queue.h"
#include "writer.h"
#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

#define bigfont Roboto13
#define smallfont Font12

class displayupdater {
  private:
    const DispImpl &display;
    threadsafe_queue<results_r> &queue;
    std::unique_ptr<UBYTE[]> image;
    std::string hostname, ip;
    std::ifstream therm;
    int ip_counter = 0;
    const writer &mywriter;
    const std::atomic<bool> &is_writing;
    std::chrono::system_clock::time_point timespecTotime(timespec ts);
    void updateIp();

  public:
    displayupdater(threadsafe_queue<results_r> &inqueue, const std::string &hostname, const std::string &ip,
                   const writer &mywriter, const std::atomic<bool> &is_writing, const DispImpl &display);
    void startUpdating();
    ~displayupdater();
};
#endif // __GIT_METEOBIKE_INCLUDE_DISPLAYUPDATER_H_

#ifndef __GIT_METEOBIKE_INCLUDE_DHTPOLLER_H_
#define __GIT_METEOBIKE_INCLUDE_DHTPOLLER_H_

#include "bcm2835.h"
#include "utils.h"
#include <atomic>
#include <mutex>
#include <tuple>

struct dhtdata_r {
    float temperature, humdidity;
};

class dhtpoller {
  private:
    std::mutex l_dht;
    const int dhtsleep;
    float temperature, humidity, t_temperature, t_humidity;
    const int pin;

  public:
    dhtpoller(const int pin, const int dhtsleep);
    ~dhtpoller();
    void startPoll(std::atomic<bool> *signal);
    std::tuple<float, float> getMeasurement();
    dhtdata_r getLatestData();
};
#endif // __GIT_METEOBIKE_INCLUDE_DHTPOLLER_H_

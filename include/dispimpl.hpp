#ifndef __GIT_METEOBIKE_INCLUDE_DISPIMPL_H_
#define __GIT_METEOBIKE_INCLUDE_DISPIMPL_H_

#include "GUI_BMPfile.h"
#include "GUI_Paint.h"
#include <iostream>
#include <memory>

class DispImpl {
  public:
    virtual ~DispImpl() {
    }
    virtual void Init() const = 0;
    virtual void Clear() const = 0;
    virtual void Display() const = 0;
    virtual void NewImage(UWORD Rotate, UWORD Color) const = 0;
    UBYTE DevInit() const;
    void ClearBuffer(UWORD Color) const;
    void ReadBmp(const std::string &path, UWORD Xstart, UWORD Ystart) const;
    void DEVDelay(UDOUBLE xms) const;
    void PaintDrawString(UWORD Xstart, UWORD Ystart, const char *pString, sFONT *Font, UWORD Color_Foreground,
                         UWORD Color_Background) const;
    void DrawLine(UWORD Xstart, UWORD Ystart, UWORD Xend, UWORD Yend, UWORD Color, DOT_PIXEL Line_width,
                  LINE_STYLE Line_Style) const;
    void DisplaySystemMessage(const std::string &line1, const std::string &line2) const;
    void DisplaySystemMessage(const std::string &line1, const std::string &line2, const std::string &line3) const;
    const UWORD Imagesize;
    const UWORD Height;
    const UWORD Width;

  protected:
    DispImpl(UWORD Imagesize, UWORD Height, UWORD Width) : Imagesize(Imagesize), Height(Height), Width(Width){};
    std::unique_ptr<UBYTE[]> Image;
};

#endif // __GIT_METEOBIKE_INCLUDE_DISPIMPL_H_
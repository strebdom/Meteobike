#ifndef __GIT_METEOBIKE_INCLUDE_METEOBIKE_H_
#define __GIT_METEOBIKE_INCLUDE_METEOBIKE_H_

#include "EPD_2in7.h"
#include "GUI_BMPfile.h"
#include "GUI_Paint.h"
#include "SHA256.h"
#include "consts.h"
#include "dispimpl.hpp"
#include "httplib.h"
#include "utils.h"
#include "writer.h"
#include <atomic>
#include <fstream>
#include <memory>
#include <string>

struct args {
    int log_every_second;
    int display_update_interval;
};

std::string getOptions(int &argc, char **argv);
void startUp(const std::string &hostname, const std::string &ip, const utils::sectionconfigmap &myargs,
             const DispImpl &display);
void showHelp();
void setupGPIO();
void shutDownScreen(const DispImpl &display);
void rebootScreen(const DispImpl &display);
bool compareSHA(const DispImpl &display, const std::string &shaweb);
void networkError(httplib::Result &result);
void uploadFiles(writer &outputw, const std::string &hostname, const std::string &psk, const DispImpl &display,
                 const utils::sectionconfigmap &myconf);
void updateBinary(const DispImpl &display);
void overwriteFile(const DispImpl &display, httplib::Client &cli);
KEYS checkEvent();
std::string getPSK(const std::string &pskpath);
void parseEvent(const KEYS &key, std::atomic<bool> &writeRecord);
#endif // __GIT_METEOBIKE_INCLUDE_METEOBIKE_H_

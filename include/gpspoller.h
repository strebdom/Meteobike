#ifndef __GIT_METEOBIKE_INCLUDE_GPSPOLLER_H_
#define __GIT_METEOBIKE_INCLUDE_GPSPOLLER_H_

#include "libgpsmm.h"
#include "utils.h"
#include <atomic>
#include <memory>
#include <mutex>
#include <string>

struct gpsdata_r {
    std::string time;
    double longitude, latitude, altitude, speed;
    bool has_fix;
    timespec_t t_time;
};

class gpspoller {
  private:
    const int kwaitingtime, gpssleep;
    std::mutex g_i_mutex;
    std::string gps_time;
    timespec_t t_time;
    bool has_fix;
    bool has_set_systemtime;
    double altitude, latitude, longitude, speed;
    const int port;
    std::unique_ptr<gpsmm> gps;
    struct gps_data_t *data;
    enum TimeFormat
    {
        LOCALTIME,
        UTC,
        UNIX,
        ISO_8601
    };

  public:
    gpspoller(const std::string &host, const int kwaitingtime, const int gpssleep, const int port = 2947);
    std::string TimespecToTimeStr(const timespec &gpsd_time, TimeFormat time_format = LOCALTIME);
    void startPoll(std::atomic<bool> *signal);
    gpsdata_r getLastData();
    ~gpspoller();
};
#endif // __GIT_METEOBIKE_INCLUDE_GPSPOLLER_H_

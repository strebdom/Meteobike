#include <iostream>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <memory>
#include <fstream>
#include <iomanip>
#include "SHA256.h"
using namespace std;

int main(int argc, char **argv)
{
    SHA256_base sha;
    std::ifstream in("bcm.tar.gz", ios::binary);
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(in), {});
    sha.update(buffer);
    auto digest = sha.digest();
    std::cout << SHA256_base::toString(digest) << std::endl;
    return EXIT_SUCCESS;
}
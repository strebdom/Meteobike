#include <bits/stdc++.h>
#include "../lib/cpp-httplib/httplib.h"

using namespace std;
int main(int argc, char *argv[])
{
	httplib::Client cli("http://meteobike.strebel.io");
	if (auto result = cli.Get("/meteobike"))
	{
		std::cout << result->status << std::endl;
		ofstream ofile("/tmp/out.bin");
		if (ofile.good())
		{
			ofile.write(result->body.c_str(), result->body.length());
		}
		else {
			exit(EXIT_FAILURE);
		}
		std::filesystem::path mypath(argv[0]);
		std::filesystem::path mylegacypath(mypath);
		mylegacypath += "old";
		std::filesystem::rename(mypath.filename(), mylegacypath.filename());
		std::filesystem::rename(std::filesystem::path("/tmp/out.bin"), mypath.filename());
		std::filesystem::permissions(mypath, std::filesystem::perms::owner_all | std::filesystem::perms::group_exec | std::filesystem::perms::group_read | std::filesystem::perms::others_exec | std::filesystem::perms::others_read);
		std::filesystem::remove(mylegacypath);
	}
	else
	{
		std::cout << result.error() << std::endl;
		exit(EXIT_FAILURE);
	}
	
	return 0;
}

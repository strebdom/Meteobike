
#include "server.h"
#include "unistd.h"
#include "utils.h"
#include <fstream>
#include <sstream>
#include <chrono>
#include <filesystem>
#include "../lib/cpp-httplib/httplib.h"
#include "plog/Log.h" // Step1: include the headers
#include "plog/Initializers/RollingFileInitializer.h"
#include "plog/Appenders/ColorConsoleAppender.h"
#include "plog/Appenders/ConsoleAppender.h"
#include "plog/Appenders/RollingFileAppender.h"
#include <plog/Formatters/TxtFormatter.h>
#include <iostream>

using namespace httplib;
namespace fs = std::filesystem;
using namespace std;

const char *html = R"(
Nothing to see here!
)";

int main(int argc, char *argv[])
{
  static plog::RollingFileAppender<plog::TxtFormatter> fileAppender("logfile.log", 8000, 3);
  static plog::ColorConsoleAppender<plog::TxtFormatter> consoleAppender;
  plog::init(plog::debug, &fileAppender).addAppender(&consoleAppender);
  auto config = utils::parseConfig("config.txt");
  auto myopts = generateConfig(config);
  PLOGI << "#########################";
  PLOGI << "Starting Server";
  PLOGI << "#########################";
  PLOGI << "Host: " << myopts.host << ":" << myopts.port;
  auto psk = getPSK(myopts.psk);
  fs::path outp{myopts.datapath};
  if (!fs::exists(outp))
  {
    PLOGE << "Error: " << outp << " doesn't exist.";
    exit(EXIT_FAILURE);
  }
  PLOGD << "PSK: " << psk;
  SSLServer svr("cert.pem", "key.pem");
  //Server svr;
  svr.Get("/", [](const Request & req, Response &res)
          { PLOGI << "GET from " << req.remote_addr << ":" << req.remote_port;
            res.set_content(html, "text/plain"); });

  svr.Post("/post", [&](const Request &req, Response &res, const ContentReader &content_reader)
           { postHandler(req, res, content_reader, psk, outp); }); 
  svr.listen(myopts.host,myopts.port);
  PLOGI << "Setup listeners";
  PLOGI << "listening to " << myopts.host << ":" << myopts.port;
}

void fileHandler(const Request &req, const ContentReader &content_reader, fs::path &outp)
{
  MultipartFormDataItems files;
  content_reader(
      [&](const MultipartFormData &file)
      {
        files.push_back(file);
        return true;
      },
      [&](const char *data, size_t data_length)
      {
        files.back().content.append(data, data_length);
        return true;
      });
  LOGI << "received " << files.size() << " files";
  for (auto &f : files)
  {
    LOGI << f.filename << " " << f.content_type << " " << f.name;
  }
  saveFiles(req, files, outp);
}

void saveFiles(const Request &req, MultipartFormDataItems &files, fs::path &outp)
{
  auto hostname = req.get_header_value("hostname");
  auto outp_sub = outp / hostname;
  if (!fs::exists(outp_sub))
  {
    fs::create_directories(outp_sub);
    PLOGI << "Created " << outp_sub;
  }
  auto now_c = chrono::system_clock::to_time_t(chrono::system_clock::now());
  auto tm = *std::localtime(&now_c);
  stringstream pp;
  pp << put_time(&tm, "%Y%m%d_%H%M%S");
  outp_sub /= pp.str();
  fs::create_directories(outp_sub);
  LOGI << "Created Upload path: " << outp_sub;
  for (auto &f : files)
  {
    auto fpath = outp_sub / f.filename;
    ofstream outfile(fpath);
    outfile.write(f.content.c_str(), f.content.length());
    LOGI << "Wrote file " << fpath << " size: " << f.content.length();
  }
}

void postHandler(const Request &req, Response &res, const ContentReader &content_reader, const string &psk, fs::path &outp)
{
  PLOGI << "Connection from " << req.remote_addr << ":" << req.remote_port;

  if (req.has_header("auth"))
  {
    auto sent_psk = req.get_header_value("auth");
    if (psk == sent_psk)
    {
      PLOGI << "Identical PSK";
      if (req.has_header("hostname"))
      {
        fileHandler(req, content_reader, outp);
        res.set_content("done", "text/plain");
      }
      else
      {
        PLOGE << "No Hostname Header";
      }
    }
    else
    {
      PLOGE << "WRONG PSK: " << sent_psk;
    }
  }
}

string getPSK(const std::string &pskpath)
{
  ifstream psk_f(pskpath);
  stringstream psk_stream;
  if (!psk_f.good())
  {
    cout << "Error, cannot read psk, exiting." << endl;
    exit(EXIT_FAILURE);
  }
  psk_stream << psk_f.rdbuf();
  auto psk = psk_stream.str();
  psk.erase(std::remove(psk.begin(), psk.end(), '\n'), psk.cend());
  return psk;
}


Opts generateConfig(const utils::configmap &myconf){  
    if (myconf.count("port")== 0 || myconf.count("host") == 0 || myconf.count("psk_path") == 0 || myconf.count("datapath") == 0){  
      PLOGE << "Error: port, host, and psk_path and datapath must be defined in config";
      exit(EXIT_FAILURE);
    }
    Opts myopts;
    myopts.port = std::stoi(myconf.at("port"));
    myopts.psk = myconf.at("psk_path");
    myopts.host = myconf.at("host");
    myopts.datapath = myconf.at("datapath");
    return myopts;
}

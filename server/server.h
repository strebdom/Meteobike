#ifndef __GIT_METEOBIKE_SCRATCH_SERVER_H_
#define __GIT_METEOBIKE_SCRATCH_SERVER_H_
#include "../lib/cpp-httplib/httplib.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <filesystem>
#include "utils.h"
namespace fs=std::filesystem;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   //define something for Windows (32-bit and 64-bit, this part is common)
   #ifdef _WIN64
      //define something for Windows (64-bit only)
   #else
      //define something for Windows (32-bit only)
   #endif
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
         // iOS, tvOS, or watchOS Simulator
    #elif TARGET_OS_MACCATALYST
         // Mac's Catalyst (ports iOS API into Mac, like UIKit).
    #elif TARGET_OS_IPHONE
        // iOS, tvOS, or watchOS device
    #elif TARGET_OS_MAC
        // Other kinds of Apple platforms
const std::string OUTPATH{"/Users/pick/MeteobikeUpload"};
    #else
    #   error "Unknown Apple platform"
    #endif
#elif __ANDROID__
    // Below __linux__ check should be enough to handle Android,
    // but something may be unique to Android.
#elif __linux__
    // linux
const std::string OUTPATH{"/root/MeteobikeUpload"};

#elif __unix__ // all unices not caught above
    // Unix
#elif defined(_POSIX_VERSION)
    // POSIX
#else
#   error "Unknown compiler"
#endif

struct Opts {
    int port;
    std::string host;
    std::string psk;
    std::string datapath;
};

void fileHandler(const httplib::Request &req, const httplib::ContentReader &content_reader, fs::path &outp);
void postHandler(const httplib::Request &req, httplib::Response &res, const httplib::ContentReader &content_reader, const std::string &psk, fs::path &outp);
void saveFiles(const httplib::Request &req, httplib::MultipartFormDataItems &files, fs::path &outp);
std::string getPSK(const std::string &pskpath);
Opts generateConfig(const utils::configmap &myconf);

#endif // __GIT_METEOBIKE_SCRATCH_SERVER_H_
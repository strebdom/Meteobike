#!/bin/bash
rm -rf build/*
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make -j 6 VERBOSE=1
cd ..
sha256sum build/meteobike > sha.txt
scp build/meteobike sha.txt root@strebel.io://var/www/vhosts/meteobike/
rm sha.txt

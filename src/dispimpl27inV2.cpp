#include "dispimpl27inV2.hpp"
#include <memory>

void DispImpl27inV2::Clear() const {
    EPD_2IN7_V2_Clear();
}

void DispImpl27inV2::Init() const {
    EPD_2IN7_V2_Init();
}

void DispImpl27inV2::Display() const {
    EPD_2IN7_V2_Display(Image.get());
}
void DispImpl27inV2::NewImage(uint16_t Rotate, uint16_t Color) const {
    Paint_NewImage(Image.get(), this->Width, this->Height, Rotate, Color);
}

DispImpl27inV2::DispImpl27inV2()
    : DispImpl(((EPD_2IN7_V2_WIDTH % 8 == 0) ? (EPD_2IN7_V2_WIDTH / 8) : (EPD_2IN7_V2_WIDTH / 8 + 1)) *
                   EPD_2IN7_V2_HEIGHT,
               EPD_2IN7_V2_HEIGHT, EPD_2IN7_V2_WIDTH) {
    Image = std::make_unique<UBYTE[]>(this->Imagesize);
}

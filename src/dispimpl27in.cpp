#include "dispimpl27in.hpp"
#include "GUI_Paint.h"
#include "dispimpl.hpp"
#include <memory>

void DispImpl27in::Clear() const {
    EPD_2IN7_Clear();
}

void DispImpl27in::Init() const {
    EPD_2IN7_Init();
}

void DispImpl27in::Display() const {
    EPD_2IN7_Display(Image.get());
}

void DispImpl27in::NewImage(uint16_t Rotate, uint16_t Color) const {
    Paint_NewImage(Image.get(), this->Width, this->Height, Rotate, Color);
}

DispImpl27in::DispImpl27in()
    : DispImpl(((EPD_2IN7_WIDTH % 8 == 0) ? (EPD_2IN7_WIDTH / 8) : (EPD_2IN7_WIDTH / 8 + 1)) * EPD_2IN7_HEIGHT,
               EPD_2IN7_HEIGHT, EPD_2IN7_WIDTH) {
    Image = std::make_unique<UBYTE[]>(this->Imagesize);
}

#include "meteobike.h"
#include "consts.h"
#include "dhtpoller.h"
#include "dispimpl.hpp"
#include "displayupdater.h"
#include "gpspoller.h"
#include "httplib.h"
#include "measurement.h"
#include "ts_queue.h"
#include "unistd.h"
#include "utils.h"
#include "writer.h"
#include <atomic>
#include <chrono>
#include <filesystem>
#include <fmt/format.h>
#include <fstream>
#include <iostream>
#include <linux/reboot.h>
#include <memory>
#include <sstream>
#include <string>
#include <sys/reboot.h>
#include <thread>

#include "GUI_BMPfile.h"
#include "GUI_Paint.h"
#include "bcm2835.h"
#include "dispimpl27in.hpp"
#include "dispimpl27inV2.hpp"

using namespace std;
using namespace std::chrono;
namespace fs = std::filesystem;

std::atomic<bool> term_signal = false;
std::atomic<bool> reboot_signal = false;
std::atomic<bool> upload_signal = false;

int main(int argc, char *argv[]) {
    // auto myargs = getOptions(argc, argv);
    auto configpath = getOptions(argc, argv);
    auto myconfig = utils::parseSectionConfig(configpath);
    cout << "Log every " << utils::getValueFromConfMap<int>(myconfig, "Interval", "measureinterval") << " second"
         << endl;
    cout << "Display update interval " << utils::getValueFromConfMap<int>(myconfig, "Interval", "displayinterval")
         << " seconds" << endl;
    if (getuid() != 0) {
        cerr << "Program must be run as root. Exiting" << endl;
        exit(EXIT_FAILURE);
    }
    DispImpl27in display;
    int internal_counter = 0;
    std::atomic<bool> writeRecords = true;
    string ip{utils::getIP()};
    string pskpath{utils::getValueFromConfMap<std::string>(myconfig, "Paths", "pskpath")};
    auto psk = getPSK(pskpath);
    string hostname{utils::getHostname()};
    cout << ip << endl;
    cout << hostname << endl;
    setupGPIO();
    startUp(hostname, ip, myconfig, display);
    dhtpoller mydht(utils::getValueFromConfMap<int>(myconfig, "GPIO", "PIN"),
                    utils::getValueFromConfMap<int>(myconfig, "Timing", "DHT_SLEEP"));
    gpspoller mygps("localhost", utils::getValueFromConfMap<int>(myconfig, "Timing", "KWAITINGTIME"),
                    utils::getValueFromConfMap<int>(myconfig, "Timing", "GPS_SLEEP"));
    writer output(utils::getValueFromConfMap<std::string>(myconfig, "Paths", "datapath"), hostname, ip);
    threadsafe_queue<results_r> tq;
    displayupdater mydsp(tq, hostname, ip, output, writeRecords, display);
    std::thread dht_t(&dhtpoller::startPoll, &mydht, &term_signal);
    std::thread gps_t(&gpspoller::startPoll, &mygps, &term_signal);
    std::thread display_t(&displayupdater::startUpdating, &mydsp);
    auto display_update_interval = utils::getValueFromConfMap<int>(myconfig, "Interval", "displayinterval");
    auto log_every_second = utils::getValueFromConfMap<int>(myconfig, "Interval", "measureinterval");
    auto t1{steady_clock::now()};
    for (;;) {
        auto dhtdata = mydht.getLatestData();
        auto gpsdata = mygps.getLastData();
        measurement mymeas(gpsdata, dhtdata);
        cout << "Humidity: " << dhtdata.humdidity << " Temperature: " << dhtdata.temperature << endl;
        cout << "#############################" << endl;
        cout << "GPS has fix?: " << gpsdata.has_fix << " Time: " << gpsdata.time << " GPS lat: " << gpsdata.latitude
             << " GPS lon: " << gpsdata.longitude << " Alt: " << gpsdata.altitude << " Speed: " << gpsdata.speed
             << endl;
        auto results = mymeas.retres();
        auto t2{steady_clock::now()};
        if (duration_cast<seconds>(t2 - t1).count() >= display_update_interval) {
            tq.push(results);
            t1 = steady_clock::now();
        }
        if (writeRecords) {
            output.createRecord(results);
            output.writeRecord();
        }
        auto event = checkEvent();
        if (event != KEYS::NOKEY) {
            parseEvent(event, writeRecords);
        }
        if (term_signal) {
            results.is_ending = true;
            tq.push(results);
            break;
        }
        std::this_thread::sleep_for(std::chrono::seconds(log_every_second));
        internal_counter++;
    }
    dht_t.join();
    gps_t.join();
    display_t.join();
    output.closeFile();
    if (reboot_signal) {
        if (upload_signal) {
            uploadFiles(output, hostname, psk, display, myconfig);
        }
        rebootScreen(display);
        display.Clear();
        bcm2835_close();
        sync();
        reboot(RB_AUTOBOOT);
    } else {
        shutDownScreen(display);
        if (auto updateEvent = checkEvent() == KEYS::KEY1) {
            updateBinary(display);
        }
        display.Clear();
        bcm2835_close();
        sync();
        reboot(RB_POWER_OFF);
    }
    return EXIT_SUCCESS;
}

void startUp(const string &hostname, const string &ip, const utils::sectionconfigmap &myargs, const DispImpl &display) {
    auto ETHLOGO = utils::getValueFromConfMap<std::string>(myargs, "Paths", "ethlogo");
    auto log_every_second = utils::getValueFromConfMap<int>(myargs, "Interval", "displayinterval");
    auto display_update_interval = utils::getValueFromConfMap<int>(myargs, "Interval", "measureinterval");
    if (display.DevInit() != 0) {
        std::cerr << "Error, cannot open activate GPIO";
        exit(EXIT_FAILURE);
    }
    display.Init();
    display.Clear();
    display.NewImage(0, WHITE);
    display.ClearBuffer(WHITE);
    display.ReadBmp(ETHLOGO, 0, 0);
    std::string compileDT{TIMESTAMP};
    display.PaintDrawString(5, 0.45 * display.Height, fmt::format("CompT: {}", compileDT).c_str(), &Font12, WHITE,
                            BLACK);
    display.Display();
    display.DEVDelay(3000);
    // EPD_2IN7_Clear();
    display.NewImage(0, WHITE);
    display.ClearBuffer(WHITE);
    display.PaintDrawString(4, 0, "Starting", &Roboto14, WHITE, BLACK);
    display.PaintDrawString(4, 0 + Roboto14.Height, fmt::format("Interval Logging: {} s", log_every_second).c_str(),
                            &Font12, WHITE, BLACK);
    display.PaintDrawString(4, 0 + Roboto14.Height + Font12.Height,
                            fmt::format("Interval Display: {} s", display_update_interval).c_str(), &Font12, WHITE,
                            BLACK);
    display.PaintDrawString(4, 60, hostname.c_str(), &Roboto12, WHITE, BLACK);
    display.PaintDrawString(4, 80, ip.c_str(), &Roboto12, WHITE, BLACK);
    display.PaintDrawString(4, display.Height - (4 * Font12.Height), "Key 1: Pause/Resume", &Font12, WHITE, BLACK);
    display.PaintDrawString(4, display.Height - (3 * Font12.Height), "Key 2: Upload/Shutdown", &Font12, WHITE, BLACK);
    display.PaintDrawString(4, display.Height - (2 * Font12.Height), "Key 3: Reboot", &Font12, WHITE, BLACK);
    display.PaintDrawString(4, display.Height - (1 * Font12.Height), "Key 4: Shutdown", &Font12, WHITE, BLACK);
    display.Display();
    display.DEVDelay(3000);
}

void setupGPIO() {
    bcm2835_init();
    bcm2835_gpio_fsel(KEYS::KEY1, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(KEYS::KEY2, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(KEYS::KEY3, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(KEYS::KEY4, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(KEYS::KEY1, BCM2835_GPIO_PUD_UP);
    bcm2835_gpio_set_pud(KEYS::KEY2, BCM2835_GPIO_PUD_UP);
    bcm2835_gpio_set_pud(KEYS::KEY3, BCM2835_GPIO_PUD_UP);
    bcm2835_gpio_set_pud(KEYS::KEY4, BCM2835_GPIO_PUD_UP);
    bcm2835_gpio_len(KEYS::KEY1);
    bcm2835_gpio_len(KEYS::KEY2);
    bcm2835_gpio_len(KEYS::KEY3);
    bcm2835_gpio_len(KEYS::KEY4);
}

void parseEvent(const KEYS &key, std::atomic<bool> &writeRecord) {
    switch (key) {
    case KEYS::KEY1: {
        cout << "Pause!" << endl;
        writeRecord = !writeRecord;
        if (writeRecord)
            cout << "write enabled" << endl;
        else
            cout << "write disabled" << endl;
        break;
    }
    case KEYS::KEY2: {
        cout << "upload file" << endl;
        term_signal = true;
        reboot_signal = true;
        upload_signal = true;
        break;
    }
    case KEYS::KEY3: {
        cout << "reboot!" << endl;
        term_signal = true;
        reboot_signal = true;
        break;
    }
    case KEYS::KEY4: {
        cout << "exit!" << endl;
        term_signal = true;
        break;
    }

    default:
        break;
    }
}

void showHelp() {
    cout << "Error: Please define -c [configfile]" << endl;
}
void shutDownScreen(const DispImpl &display) {
    display.NewImage(0, WHITE);
    display.ClearBuffer(WHITE);
    display.PaintDrawString(0, 0, "Shutdown Meteobike", &Roboto12, WHITE, BLACK);
    display.PaintDrawString(0, 0 + 2 * Roboto12.Height, "Please wait for screen to be cleared.", &Roboto12, WHITE,
                            BLACK);
    display.Display();
    std::this_thread::sleep_for(3s);
}

void rebootScreen(const DispImpl &display) {
    display.NewImage(0, WHITE);
    display.ClearBuffer(WHITE);
    display.PaintDrawString(0, 0, "Reboot Meteobike", &Roboto12, WHITE, BLACK);
    display.PaintDrawString(0, 0 + 2 * Roboto12.Height, "Please wait for screen to be cleared.", &Roboto12, WHITE,
                            BLACK);
    display.Display();
    std::this_thread::sleep_for(3s);
}

void uploadFiles(writer &outputw, const string &hostname, const string &psk, const DispImpl &display,
                 const utils::sectionconfigmap &myconf) {
    display.DisplaySystemMessage("Upload Meteobike data", "Please wait while connecting.");
    outputw.closeFile();
    httplib::SSLClient cli("meteobike.strebel.io", 12345);
    cli.enable_server_certificate_verification(false);
    auto res = cli.Get("/");
    if (!res) {
        std::cout << res.error() << std::endl;
        std::stringstream err;
        err << res.error();
        display.DisplaySystemMessage("Couldn't connect", err.str());
        std::this_thread::sleep_for(3s);
        EPD_2IN7_Clear();
        exit(EXIT_FAILURE);
    }
    fs::path outp{utils::getValueFromConfMap<std::string>(myconf, "Paths", "datapath")};
    auto now_c = chrono::system_clock::to_time_t(chrono::system_clock::now());
    auto tm = *std::localtime(&now_c);
    stringstream pp;
    pp << put_time(&tm, "UPLOAD_%Y%m%d_%H%M%S");
    fs::path mvpp{utils::getValueFromConfMap<std::string>(myconf, "Paths", "archivepath")};
    mvpp /= (pp.str());
    fs::create_directories(mvpp);
    httplib::MultipartFormDataItems items;
    for (auto const &dir_entry : fs::directory_iterator{outp}) {
        if (dir_entry.is_regular_file()) {
            ifstream measurefile(dir_entry.path());
            if (measurefile.good()) {
                stringstream fc;
                fc << measurefile.rdbuf();
                items.push_back({dir_entry.path().filename(), fc.str(), dir_entry.path().filename(), ""});
            }
            fs::rename(dir_entry.path(), (mvpp / dir_entry.path().filename()));
        }
    }
    httplib::Headers headers = {{"hostname", hostname}, {"auth", psk}};
    cli.set_default_headers(headers);
    cli.Post("/post", items);
}

void updateBinary(const DispImpl &display) {
    std::string shasum;
    display.DisplaySystemMessage("Update Meteonbike", "http://meteobike.strebel.io/meteobike");
    httplib::Client cli("http://meteobike.strebel.io");
    if (auto result = cli.Get("/sha.txt")) {
        auto sha = stringstream{result->body};
        sha >> shasum;

    } else {
        display.DisplaySystemMessage("Couldn't update Meteobike", "Network Connection?",
                                     httplib::to_string(result.error()));
        std::this_thread::sleep_for(3s);
        EPD_2IN7_Clear();
    }
    if (compareSHA(display, shasum)) {
        overwriteFile(display, cli);
    }
}

void overwriteFile(const DispImpl &display, httplib::Client &cli) {
    if (auto result = cli.Get("/meteobike")) {
        std::cout << result->status << std::endl;
        ofstream ofile("/tmp/out.bin");
        if (ofile.good()) {
            ofile.write(result->body.c_str(), result->body.length());
        } else {
            display.DisplaySystemMessage("Couldn't update Meteobike", "File Error");
            std::this_thread::sleep_for(3s);
            display.Clear();
            return;
        }
        std::filesystem::path mypath("/usr/local/bin/meteobike");
        std::filesystem::path mylegacypath(mypath);
        mylegacypath += "old";
        std::filesystem::rename(mypath, mylegacypath);
        std::filesystem::rename(std::filesystem::path("/tmp/out.bin"), mypath);
        std::filesystem::permissions(mypath, std::filesystem::perms::owner_all | std::filesystem::perms::group_exec |
                                                 std::filesystem::perms::group_read |
                                                 std::filesystem::perms::others_exec |
                                                 std::filesystem::perms::others_read);
        std::filesystem::remove(mylegacypath);
        display.DisplaySystemMessage("Meteobike was updated",
                                     fmt::format("{} bytes written", result->body.length()).c_str());
        std::this_thread::sleep_for(3s);
    } else {
        display.DisplaySystemMessage("Couldn't update Meteobike", "Network Connection?",
                                     httplib::to_string(result.error()));
        std::this_thread::sleep_for(3s);
        EPD_2IN7_Clear();
    }
}

KEYS checkEvent() {
    if (bcm2835_gpio_eds(KEYS::KEY1)) {
        bcm2835_gpio_set_eds(KEYS::KEY1);
        cout << "Event Key 1 detected" << endl;
        return KEYS::KEY1;
    }
    if (bcm2835_gpio_eds(KEYS::KEY2)) {
        bcm2835_gpio_set_eds(KEYS::KEY2);
        cout << "Event Key 2 detected" << endl;
        return KEYS::KEY2;
    }
    if (bcm2835_gpio_eds(KEYS::KEY3)) {
        bcm2835_gpio_set_eds(KEYS::KEY3);
        cout << "Event Key 3 detected" << endl;
        return KEYS::KEY3;
    }
    if (bcm2835_gpio_eds(KEYS::KEY4)) {
        bcm2835_gpio_set_eds(KEYS::KEY4);
        cout << "Event Key 4 detected" << endl;
        return KEYS::KEY4;
    }
    return KEYS::NOKEY;
}

string getPSK(const std::string &pskpath) {
    ifstream psk_f(pskpath);
    stringstream psk_stream;
    if (!psk_f.good()) {
        cout << "Error, cannot read pskfile, exiting." << endl;
        exit(EXIT_FAILURE);
    }
    psk_stream << psk_f.rdbuf();
    auto psk = psk_stream.str();
    psk.erase(std::remove(psk.begin(), psk.end(), '\n'), psk.cend());
    return psk;
}

std::string getOptions(int &argc, char **argv) {
    std::string path;
    int opt;
    while ((opt = getopt(argc, argv, "c:")) != -1) {
        switch (opt) {
        case 'c':
            path = optarg;
            break;
        default:
            showHelp();
            exit(EXIT_FAILURE);
        }
    }
    if (path == "") {
        showHelp();
        exit(EXIT_FAILURE);
    }
    return path;
}
bool compareSHA(const DispImpl &display, const std::string &shaweb) {
    SHA256_base sha;
    ifstream b_meteobike("/usr/local/bin/meteobike");
    if (!b_meteobike.good()) {
        display.DisplaySystemMessage("Error", "Can't  open /usr/local/bin/meteobike");
        exit(EXIT_FAILURE);
    }
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(b_meteobike), {});
    sha.update(buffer);
    auto digest = sha.digest();
    auto sha_meteobike_local = SHA256_base::toString(digest);
    if (sha_meteobike_local != shaweb) {
        display.DisplaySystemMessage("Hash value different, updating Executable", sha_meteobike_local, shaweb);
        std::this_thread::sleep_for(3s);
    } else {
        display.DisplaySystemMessage("Hash value identical, not updating Executable", sha_meteobike_local, shaweb);
        std::this_thread::sleep_for(3s);
    }
    return sha_meteobike_local != shaweb;
}
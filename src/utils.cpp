#include "utils.h"
#include "pi_2_dht_read.h"

#include <algorithm>
#include <arpa/inet.h>
#include <cctype>
#include <cstring>
#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits.h>
#include <locale>
#include <ranges>
#include <regex>
#include <string>
#include <sys/socket.h>
#include <unistd.h>
#include <vector>

namespace utils {

std::string getIP() {
    int sock = socket(PF_INET, SOCK_DGRAM, 0);
    sockaddr_in loopback;

    if (sock == -1) {
        std::cerr << "Could not socket\n";
        return std::string("None");
    }

    memset(&loopback, 0, sizeof(loopback));
    loopback.sin_family = AF_INET;
    loopback.sin_addr.s_addr = 1337; // can be any IP address
    loopback.sin_port = htons(9);    // using debug port

    if (connect(sock, reinterpret_cast<sockaddr *>(&loopback), sizeof(loopback)) == -1) {
        close(sock);
        std::cerr << "Could not connect\n";
        return std::string("None");
    }

    socklen_t addrlen = sizeof(loopback);
    if (getsockname(sock, reinterpret_cast<sockaddr *>(&loopback), &addrlen) == -1) {
        close(sock);
        std::cerr << "Could not getsockname\n";
        return std::string("None");
    }

    close(sock);

    char buf[INET_ADDRSTRLEN];
    if (inet_ntop(AF_INET, &loopback.sin_addr, buf, INET_ADDRSTRLEN) == 0x0) {
        std::cerr << "Could not inet_ntop\n";
        return std::string("None");
    }
    return std::string(buf);
}

std::string getHostname() {
    char hname[HOST_NAME_MAX];
    int error = 0;
    if (gethostname(hname, HOST_NAME_MAX) == -1) {
        std::cout << "Error, cannot get hostname" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::string(hname);
}

sectionconfigmap parseSectionConfig(std::string filename) {
    std::regex secregex(R"(\[([a-zA-Z0-9 _-]*)\])", std::regex_constants::ECMAScript);
    std::ifstream conffile(filename);
    unsigned int sectioncounter = 0;
    if (!conffile.good()) {
        throw std::runtime_error("Cannot open config file");
    }
    std::map<std::string, std::variant<std::string, int, double>> *currentsection;
    sectionconfigmap myconf;
    std::string line;
    std::smatch secmatch;
    while (std::getline(conffile, line)) {
        trim(line);
        if (line[0] == '#' || line.empty() || line[0] == ';')
            continue;
        if (std::regex_search(line, secmatch, secregex)) {
            myconf.emplace(secmatch.str(1), std::map<std::string, std::variant<std::string, int, double>>());
            currentsection = &myconf[secmatch.str(1)];
            sectioncounter++;
            continue;
        }
        auto comment = line.find("#");
        if (comment != std::string::npos) {
            line = line.substr(0, comment);
        }
        auto equal = line.find("=");
        if (equal == std::string::npos)
            continue;
        auto key = line.substr(0, equal);
        auto value = line.substr(equal + 1, line.size());
        trim(value);
        trim(key);
        if (sectioncounter > 0) {
            if (detectNumber(value)) {
                currentsection->emplace(key, std::stof(value));
            } else {
                currentsection->emplace(key, value);
            }
        }
    }
    return myconf;
}

configmap parseConfig(std::string filename) {
    std::ifstream conffile(filename);
    if (!conffile.good()) {
        throw std::runtime_error("Cannot open config file");
    }
    configmap myconf;
    std::string line;
    while (std::getline(conffile, line)) {
        trim(line);
        if (line[0] == '#' || line.empty())
            continue;
        auto comment = line.find("#");
        if (comment != std::string::npos) {
            line = line.substr(0, comment);
        }
        int equal = line.find("=");
        if (equal == std::string::npos)
            continue;
        auto key = line.substr(0, equal);
        auto value = line.substr(equal + 1, line.size());
        trim(value);
        trim(key);
	myconf.emplace(key, value);
    }
    return myconf;
}
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

bool detectNumber(const std::string &str) {
    std::regex numreg(R"(^[-+]?([0-9]+(\.[0-9]*)?|\.[0-9]+)([eE][-+]?[0-9]+)?$)", std::regex_constants::ECMAScript);
    if (std::regex_search(str, numreg))
        return true;
    else
        return false;
}

template <> int getValueFromConfMap<int>(const sectionconfigmap &conf, std::string section, std::string key) {
    return static_cast<int>(std::get<double>(conf.at(section).at(key)));
}
} // namespace utils

#include "displayupdater.h"
#include "fonts.h"
#include "utils.h"
#include <chrono>
#include <fmt/chrono.h>
#include <fmt/format.h>

using namespace std;

displayupdater::displayupdater(threadsafe_queue<results_r> &inqueue, const std::string &hostname, const std::string &ip,
                               const writer &mywriter, const std::atomic<bool> &is_writing, const DispImpl &display)
    : queue(inqueue), ip(ip), hostname(hostname), mywriter(mywriter), is_writing(is_writing), display(display) {
    if (ip == "None") {
        updateIp();
    }
    stringstream therm_ss;
    ifstream therm("/sys/class/thermal/thermal_zone0/temp");
    if (!therm.is_open()) {
        cerr << "Couldn't open /sys/class/thermal/thermal_zone0/temp, exiting!";
        exit(EXIT_FAILURE);
    }
    therm_ss << therm.rdbuf();
    therm.close();
    auto i_therm = std::stof(therm_ss.str()) / 1000;
    display.Clear();
    display.NewImage(0, WHITE);
    display.ClearBuffer(WHITE);
    display.PaintDrawString(4, 0, "T: NaN C", &bigfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 1 * (bigfont.Height), "RH: NaN[%]", &bigfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 2 * (bigfont.Height), "pv: NaN[kpa]", &bigfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 3 * (bigfont.Height), "Speed: NaN[m/s]", &bigfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 4 * (bigfont.Height), "Altitude: NaN[m]", &bigfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 5 * (bigfont.Height), "Date/time: NaN", &bigfont, WHITE, BLACK);
    display.DrawLine(4, 6 * (bigfont.Height), display.Width, 6 * (bigfont.Height), BLACK, DOT_PIXEL_1X1,
                     LINE_STYLE_SOLID);
    display.PaintDrawString(4, 0 + 6 * (bigfont.Height) + smallfont.Height, hostname.c_str(), &smallfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 6 * (bigfont.Height) + 2 * (smallfont.Height), ip.c_str(), &smallfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 6 * (bigfont.Height) + 3 * (smallfont.Height),
                            fmt::format("Records: {}", mywriter.counter).c_str(), &smallfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 6 * (bigfont.Height) + 4 * (smallfont.Height),
                            fmt::format("Recording: {}", is_writing).c_str(), &smallfont, WHITE, BLACK);
    display.PaintDrawString(4, 0 + 6 * (bigfont.Height) + 5 * (smallfont.Height),
                            fmt::format("CPU Temp: {}[C]", i_therm).c_str(), &smallfont, WHITE, BLACK);
    display.Display();
    display.DEVDelay(3000);
}

void displayupdater::startUpdating() {
    for (;;) {
        results_r res;
        queue.wait_and_pop(res);
        if (res.is_ending)
            break;
        if (ip == "None" && ip_counter < 5) {
            updateIp();
        }
        stringstream therm_ss;
        therm.open("/sys/class/thermal/thermal_zone0/temp");
        therm_ss << therm.rdbuf();
        auto i_therm = std::stof(therm_ss.str()) / 1000;
        therm.close();
        // EPD_2IN7_Clear();
        display.NewImage(0, WHITE);
        display.ClearBuffer(WHITE);
        display.PaintDrawString(4, 0, fmt::format("T: {}[C]", res.dht.temperature).c_str(), &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 1 * (bigfont.Height), fmt::format("RH: {}[%]", res.dht.humdidity).c_str(),
                                &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 2 * (bigfont.Height), fmt::format("pv: {:.3}[kpa]", res.pv / 1000).c_str(),
                                &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 3 * (bigfont.Height), fmt::format("Speed: {:.2}[m/s]", res.gps.speed).c_str(),
                                &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 4 * (bigfont.Height), fmt::format("Altitude:{}[m]", res.gps.altitude).c_str(),
                                &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 5 * (bigfont.Height), "Date/time:", &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 6 * (bigfont.Height),
                                fmt::format("{:%d-%m-%y %H:%M:%S}", timespecTotime(res.gps.t_time)).c_str(), &bigfont,
                                WHITE, BLACK);
        display.PaintDrawString(4, 0 + 7 * (bigfont.Height), fmt::format("Lat: {:.5}", res.gps.latitude).c_str(),
                                &bigfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 8 * (bigfont.Height), fmt::format("Lon: {:.5}", res.gps.longitude).c_str(),
                                &bigfont, WHITE, BLACK);
        display.DrawLine(4, 9 * (bigfont.Height), display.Width, 9 * (bigfont.Height), BLACK, DOT_PIXEL_1X1,
                         LINE_STYLE_SOLID);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + smallfont.Height, hostname.c_str(), &smallfont, WHITE,
                                BLACK);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + 2 * (smallfont.Height), ip.c_str(), &smallfont, WHITE,
                                BLACK);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + 3 * (smallfont.Height),
                                fmt::format("GPS has fix?: {}", res.gps.has_fix).c_str(), &smallfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + 4 * (smallfont.Height),
                                fmt::format("Records: {}", mywriter.counter).c_str(), &smallfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + 5 * (smallfont.Height),
                                fmt::format("Recording: {}", is_writing).c_str(), &smallfont, WHITE, BLACK);
        display.PaintDrawString(4, 0 + 9 * (bigfont.Height) + 6 * (smallfont.Height),
                                fmt::format("CPU Temp: {}[C]", i_therm).c_str(), &smallfont, WHITE, BLACK);
        display.Display();
    }
}

std::chrono::system_clock::time_point displayupdater::timespecTotime(timespec_t ts) {

    auto duration = std::chrono::seconds{ts.tv_sec} + std::chrono::nanoseconds{ts.tv_nsec};

    auto dur = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
    return std::chrono::system_clock::time_point(dur);
}

void displayupdater::updateIp() {
    ip = utils::getIP();
    ip_counter++;
}

displayupdater::~displayupdater() {
}

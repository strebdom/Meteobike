#include "writer.h"
#include "fmt/format.h"
#include <chrono>
#include <ctime>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <sstream>
using namespace std;

writer::~writer() {
    ofile.close();
}

writer::writer(const string &path, const string &hostname, const string &ip)
    : path(path), hostname(hostname), ip(ip), diritems(0), record("") {
    if (!filesystem::exists(path)) {
        exit(EXIT_FAILURE);
        cout << "Path " << path << " does not exist." << endl;
    }
    for (auto const &dir_entry : filesystem::directory_iterator{path}) {
        diritems++;
    }
    cout << diritems << " directory items in " << path << endl;
    stringstream path_s;
    path_s << path << "/" << std::setfill('0') << std::setw(4) << diritems << "_" << hostname << ".csv";
    cout << path_s.str() << endl;
    ofile.open(path_s.str());
    if (!ofile.is_open()) {
        cout << "File " << path_s.str() << " could not be opened." << endl;
        exit(EXIT_FAILURE);
    }
    writeHeader();
}

void writer::writeHeader() {
    ofile << "Hostname,count,GPS_Time,Altitude,Latitude,Longitude,Speed,Temperature,RelHumidity,VapourPressure,"
             "WetBulbTemp,Heatindex\n";
    ofile.flush();
}

void writer::createRecord(const results_r &mymeas) {
    record = fmt::format("{},{},{},{},{:.7},{:.7},{},{},{},{},{},{}", hostname, counter, mymeas.gps.time,
                         mymeas.gps.altitude, mymeas.gps.latitude, mymeas.gps.longitude, mymeas.gps.speed,
                         mymeas.dht.temperature, mymeas.dht.humdidity, mymeas.pv, mymeas.wbtemp, mymeas.heatindex);
    /*record.str("");
    record << hostname << ","
           << counter << ","
           << mymeas.gps.time << ","
           << mymeas.gps.altitude << ","
           << mymeas.gps.latitude << ","
           << mymeas.gps.longitude << ","
           << mymeas.gps.speed << ","
           << mymeas.dht.temperature << ","
           << mymeas.dht.humdidity << ","
           << mymeas.pv << ","
           << mymeas.wbtemp << ","
           << mymeas.heatindex;*/
}

void writer::writeRecord() {
    ofile << record << "\n";
    ofile.flush();
    counter++;
}

void writer::closeFile() {
    ofile.close();
}

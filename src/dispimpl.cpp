#include "dispimpl.hpp"
#include "DEV_Config.h"
#include "Debug.h"
#include "GUI_BMPfile.h"
#include "GUI_Paint.h"

void DispImpl::DEVDelay(uint32_t xms) const {
    DEV_Delay_ms(xms);
}

UBYTE DispImpl::DevInit() const {
    return DEV_Module_Init();
}

void DispImpl::ClearBuffer(uint16_t Color) const {
    Paint_Clear(Color);
}

void DispImpl::ReadBmp(const std::string &path, uint16_t Xstart, uint16_t Ystart) const {
    GUI_ReadBmp(path.c_str(), Xstart, Ystart);
}

void DispImpl::PaintDrawString(uint16_t Xstart, uint16_t Ystart, const char *pString, sFONT *Font,
                               uint16_t Color_Foreground, uint16_t Color_Background) const {
    Paint_DrawString_EN(Xstart, Ystart, pString, Font, Color_Foreground, Color_Background);
}

void DispImpl::DrawLine(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t Yend, uint16_t Color,
                        DOT_PIXEL Line_width, LINE_STYLE Line_Style) const {
    Paint_DrawLine(Xstart, Ystart, Xend, Yend, Color, Line_width, Line_Style);
}

void DispImpl::DisplaySystemMessage(const std::string &line1, const std::string &line2) const {
    NewImage(0, WHITE);
    ClearBuffer(WHITE);
    PaintDrawString(0, 0, line1.c_str(), &Font12, WHITE, BLACK);
    PaintDrawString(0, 0 + 2 * Font12.Height, line2.c_str(), &Font12, WHITE, BLACK);
    Display();
}

void DispImpl::DisplaySystemMessage(const std::string &line1, const std::string &line2,
                                    const std::string &line3) const {
    NewImage(0, WHITE);
    ClearBuffer(WHITE);
    PaintDrawString(0, 0, line1.c_str(), &Font12, WHITE, BLACK);
    PaintDrawString(0, 0 + 4 * Font12.Height, line2.c_str(), &Font12, WHITE, BLACK);
    Paint_DrawString_EN(0, 0 + 8 * Font12.Height, line3.c_str(), &Font12, WHITE, BLACK);
    Display();
}
